defmodule HlBudget.Repo do
  use Ecto.Repo,
    otp_app: :hl_budget,
    adapter: Ecto.Adapters.Postgres
end
