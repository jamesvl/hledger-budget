defmodule HlBudget.Commodities do
  alias HlBudget.Commodities.Commodity

  def import_from_api(commodity_data) do
    cs = Commodity.changeset(%Commodity{}, commodity_data)

    if cs.valid? do
      {:ok, data} = Ecto.Changeset.apply_action(cs, :insert)

      data
    else
      cs
    end
  end
end
