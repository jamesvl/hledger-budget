defmodule HlBudget.Commodities.Commodity do
  use Ecto.Schema
  import Ecto.Changeset

  alias HlBudget.Accounts.Account

  schema "commodities" do
    field :commodity, :string
    field :is_multiplier, :boolean

    field :price, :map
    field :quantity, :decimal

    field :style, :map

    belongs_to :account, Account
  end

  @allowed ~w(commodity is_multiplier price quantity style)a

  def changeset(orig, attrs) do
    orig
    |> cast(api_to_local_names(attrs), @allowed)
    |> validate_required(@allowed)
  end

  def api_to_local_names(attrs) do
    %{
      "commodity" => attrs["acommodity"],
      "is_multiplier" => attrs["aismultiplier"],
      "price" => attrs["aprice"],
      "quantity" => attrs["aquantity"],
      "style" => attrs["astyle"]
    }
  end
end
