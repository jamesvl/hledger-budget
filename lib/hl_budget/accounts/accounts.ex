defmodule HlBudget.Accounts do
  alias HlBudget.Accounts.Account
  alias HlBudget.Commodities

  def import_accounts_from_api(api_data) do
    Enum.map(api_data, &import_account(&1))
  end

  def import_account(acct) do
    %Account{
      name: acct["aname"],
      boring: acct["aboring"],
      num_postings: acct["anumpostings"],
      parent_name: acct["aparentname"],
      ebalance: Enum.map(acct["aebalance"], &Commodities.import_from_api(&1)),
      ibalance: Enum.map(acct["aibalance"], &Commodities.import_from_api(&1)),
      subs: Enum.map(acct["asubs"], &import_account(&1))
    }
  end
end
