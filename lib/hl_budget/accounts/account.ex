defmodule HlBudget.Accounts.Account do
  use Ecto.Schema
  # import Ecto.Changeset

  alias HlBudget.Accounts.Account
  alias HlBudget.Commodities.Commodity

  schema "accounts" do
    field :name, :string
    field :boring, :boolean

    field :num_postings, :integer
    field :parent_name, :string

    # balance exclusive of all sub-accounts
    field :ebalance, {:array, :map}
    # balance inclusive of all sub-accounts
    field :ibalance, {:array, :map}

    belongs_to :parent, Account
    has_many :subs, Account, foreign_key: :parent_id

    has_many :commodities, Commodity
  end
end
