defmodule HlBudgetWeb.PageController do
  use HlBudgetWeb, :controller

  alias HlBudget.Accounts

  def index(conn, _params) do
    {accts, msg} =
      case HTTPoison.get("http://localhost:8001/api/v1/accounts") do
        {:ok, %{status_code: 200, body: body}} ->
          accts =
            Jason.decode!(body)
            |> Accounts.import_accounts_from_api()

          # IO.inspect(List.first(accts))

          {accts, nil}
        err ->
          IO.inspect(err)
          {nil, "API not found - are you running <code>hledger-api</code>?"}
      end

    render(conn, "index.html", accts: accts, msg: msg)
  end
end
