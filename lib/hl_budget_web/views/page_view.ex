defmodule HlBudgetWeb.PageView do
  use HlBudgetWeb, :view

  def name_no_parent(%{name: name, parent_name: nil}), do: name

  def name_no_parent(%{name: name, parent_name: par}) do
    String.replace_prefix(name, par <> ":", "")
  end

  def list_account(a) do
    ~E"""
    <li>
      <%= name_no_parent(a) %>
      <%= print_commodity(a.ebalance) %>
      <%= if a.num_postings > 0 do %>
        <br><span class="has-text-grey">(<%= a.num_postings %> postings)</span>
      <% end %>
      <%= if a.subs do %>
        <ul>
          <%= for sa <- a.subs, do: list_account(sa) %>
        </ul>
      <% end %>
    </li>
    """
  end

  def print_commodity([]), do: nil

  def print_commodity([commodity]) do
    {:safe, Number.Currency.number_to_currency(commodity.quantity)}
  end

  def print_commodity(_), do: nil
end
