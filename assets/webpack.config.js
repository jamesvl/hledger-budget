const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const devMode = process.env.NODE_ENV !== 'production';

const paths = {
  static: path.join(__dirname, "../priv/static"),
  build: path.join(__dirname, "../priv/static"),
  node_modules: path.join(__dirname, "./node_modules"),
  src: path.join(__dirname, "./"),
}

module.exports = (env, options) => ({
  optimization: {
    minimizer: [
      new UglifyJsPlugin({ cache: true, parallel: true, sourceMap: false }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  entry: {
    'app': ['./js/app.js'].concat(glob.sync('./vendor/**/*.js')),
    'css': path.join(paths.src, "css/app.scss"),
  },
  output: {
    filename: "js/[name].js",
    path: paths.static,
    publicPath: paths.static,
    // path: path.resolve(__dirname, '../priv/static/js')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        //test: /\.(sa|sc|c)ss$/,
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            //loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: "css-loader", options: {
              sourceMap: devMode
            }
          },
          'postcss-loader',
          {
            loader: "sass-loader", options: {
              sourceMap: devMode
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: 'css/app.css' }),
    new CopyWebpackPlugin([{from: path.join(paths.src, 'static'), to: paths.static}])
  ],
  watchOptions: {
    ignored: ['node_modules/**', '**/.git/**']
  },
});
